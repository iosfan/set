//
//  ViewController.swift
//  Set
//
//  Created by paul on 09/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private lazy var game = Game(cardCellsOnTable: cardButtons.count)

    @IBOutlet weak var deckLabel: UILabel!

    @IBOutlet weak var setLabel: UILabel!

    @IBOutlet weak var scoreLabel: UILabel!

    @IBOutlet weak var newGameButton: UIButton!

    @IBOutlet weak var dealButton: UIButton!

    @IBOutlet var cardButtons: [UIButton]!
    
    @IBAction func newGame(_ sender: UIButton) {
        game = Game(cardCellsOnTable: cardButtons.count)
        updateViewFromModel()
    }

    @IBAction func selectCard(_ sender: UIButton) {
        if let index = cardButtons.firstIndex(of: sender) {
            game.selectCard(with: index)
        }
        updateViewFromModel()
    }

    @IBAction func deal3Cards(_ sender: UIButton) {
        game.deal3Cards()
        updateViewFromModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        newGameButton.layer.cornerRadius = 16
        newGameButton.layer.borderColor = #colorLiteral(red: 0.6630887985, green: 0.9985001683, blue: 0.09665273875, alpha: 1)
        newGameButton.layer.borderWidth = CGFloat(3)
        dealButton.layer.cornerRadius = 16
        dealButton.layer.borderColor = #colorLiteral(red: 0.6630887985, green: 0.9985001683, blue: 0.09665273875, alpha: 1)
        dealButton.layer.borderWidth = CGFloat(3)

        updateViewFromModel()
    }
    
    private func updateViewFromModel() {
        cardButtons.forEach {
            $0.alpha = 0
            $0.layer.cornerRadius = 16
            $0.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            $0.layer.borderWidth = CGFloat(3)
        }

        for position in 0..<game.cardsOnTable.count {
            renderTitle(for: game.cardsOnTable[position], on: cardButtons[position])
        }
        
        deckLabel.text = "Deck: \(game.deck.count)"
        scoreLabel.text = "Score: \(game.score)"
        setLabel.text = game.isSet ? "Set!" : "Not Set!"

        cardButtons.forEach { $0.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) }
        for selectedCard in game.selectedCards {
            if let positionOnTable = game.cardsOnTable.firstIndex(of: selectedCard) {
                cardButtons[positionOnTable].layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
        }
        
        dealButton.isEnabled = game.deck.count > 0 && (!game.isTableFull || game.isSet)
    }
    
    private func renderTitle(for card: Card, on button: UIButton) {
        let alpha = card.shading == .striped ? 0.25 : 1
        let strokeWidth = card.shading == .open ? 5.0 : -5.0
        let attributes: [NSAttributedString.Key : Any] = [
            .strokeWidth: strokeWidth,
            .strokeColor: getUIColor(for: card.color),
            .foregroundColor: getUIColor(for: card.color).withAlphaComponent(CGFloat(alpha))
        ]
        let title = String(repeating: card.shape.rawValue, count: card.quantity)
        let cardTitle = NSAttributedString(string: title, attributes: attributes)

        button.setAttributedTitle(cardTitle, for: UIControl.State.normal)
        button.alpha = 1
    }
}

extension ViewController {
    func getUIColor(for color: Color) -> UIColor {
        switch color {
        case .red:
            return UIColor.red
        case .green:
            return UIColor.green
        case .purple:
            return UIColor.purple
        }
    }
}
