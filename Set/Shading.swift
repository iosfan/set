//
//  Shading.swift
//  Set
//
//  Created by paul on 15/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import Foundation

enum Shading: CaseIterable {
    case solid, striped, open
}
