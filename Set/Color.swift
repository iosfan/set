//
//  Color.swift
//  Set
//
//  Created by paul on 15/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import Foundation

enum Color: CaseIterable {
    case red, green, purple
}
